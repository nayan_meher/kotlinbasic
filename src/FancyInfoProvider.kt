class FancyInfoProvider : BasicInfoProvider(){
    override val infoProvider: String
        get() = "Fancy info provider"
    override val sessionId: String
        get() = "fancySession"


    override fun printInfo(person: Person) {
        super.printInfo(person)
        println("Fancy info")
    }
}