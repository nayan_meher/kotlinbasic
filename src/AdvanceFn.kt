
fun filterList(list: List<String>, fu1: ((String) -> Boolean)?){
    list.forEach {
        if (fu1?.invoke(it)==true){
            println(it)
        }
    }


}

val fun2: (String)-> Boolean = {it.startsWith("j")}

fun fun3(str: String):Boolean{
    if (str.startsWith("j")){
        return true
    }
    return false
}

fun fun4(): (String)->Boolean{
    return {it.startsWith("k")}
}

fun main() {
    val list = listOf("kotlin",null, "java", "javascript", "c++")
    //filterList(list, fun4())

    //filterList(list, null)
    val map = list
            .filterNotNull()
            .associate {
                it to it.length
            }

    val language = list.first()
    println()
}