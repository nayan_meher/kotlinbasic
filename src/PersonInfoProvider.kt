interface PersonInfoProvider {
    val  infoProvider: String

    fun printInfo(person: Person){
        println(infoProvider)
        person.showInfo()
    }
}

interface SessionInfoProvider{
    val getSessionId: String
}

open class BasicInfoProvider : PersonInfoProvider, SessionInfoProvider{
    override val infoProvider: String
        get() = "newPersonInfo"

    protected  open val sessionId: String = "Session_id"

    override fun printInfo(person: Person) {
        super.printInfo(person)
        println("new added info to basic info")
    }

    override val getSessionId: String
        get() = sessionId
    fun showSessionId(){
        println("The provided session id : $getSessionId")
    }
}

fun checkType(provider: PersonInfoProvider){
    if (provider !is SessionInfoProvider){
        println(" provider is not session info provider")
    }else{
        println(" provider is session info provider")
        (provider as SessionInfoProvider).getSessionId
    }

}

fun main() {
    var personInfo = object : PersonInfoProvider{
        override val infoProvider: String
            get() = "new info provider"
    }


    personInfo.printInfo(Person())
    //personInfo.showSessionId()

    checkType(personInfo)
}