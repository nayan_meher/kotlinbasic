/*

var name = "nayan"
var greeting: String? = null
*/

fun getGreeting(name: String ): String{
    return "hello $name"
}
//single expression function
fun getName() = "noob master"

fun sayHi(name: String)= println("hello $name") //one line function

fun main() {
    val books = arrayOf("c", "java", "kotlin")
    var cars = listOf<String>("mustang", "figo", "eco sports") // immuable class
    var cars2 = mutableListOf<String>("mustang", "figo", "eco sports") //mutable class
    cars2.add("endaviour")

    var subjects = mapOf(1 to "math", 2 to "science", 3 to "english")

    //class
    var person = Person()
    person.firstname
    person.lastname

    //person.nickName = "nim"
    //println(person.nickName)
    //person.showInfo()


    //passing a collection as a parameter
    /*showCollection(cars)
    showCollection(cars2)*/

    //passing a list to vararg
   // showList("car", "book", "lib","nayan", name = "boss")

    //passing a list to vararg
    //showList(*books, name = "noob") //named parameter

    //greetings(name = "nayan", greet = "hello")// we can change the order of parameter in named parameter

    /*  println(books.size)
    println(books[2])*/
/*
    for (book in books){
        println(book)
    }*/
 /*   books.forEach {
        println(it)
    }*/
/*
    books.forEachIndexed { index, s ->
        println("for index $index the item is $s")
    }

    cars.forEach { car ->
        println(car)
    }
    cars2.forEach { car ->
        println(car)
    }

    subjects.forEach { key, value ->
        println("$key -> $value")
    }*/

}
/*
            fun showList(vararg list:String, name: String){
                list.forEach{
                    println("name ane $it")
                }
            }*/

/*fun showCollection(collName: List<String>){
    collName.forEach { collNameitem->
        println(collNameitem)
    }
}*/

/*fun greetings(greet: String = "hi", name: String= "kevin") = println("$greet $name")*/


