import EntityType.*
import java.util.*

enum class EntityType{
    HELP, NOOB, PRO, LEGEND;
    fun getFormatedName() = name.toLowerCase().capitalize()
}

object EntityFactory {
    fun getEntity(type: EntityType): Entity{
        val id = UUID.randomUUID().toString()
        val name = when(type){
            NOOB -> "noob"
            PRO -> type.name
            LEGEND -> type.getFormatedName()
            HELP -> type.getFormatedName()
        }

        return when(type){
            NOOB -> Entity.Noob(id, name)
            PRO -> Entity.Pro(id, name)
            LEGEND -> Entity.Legend(id, name, 2f)
            HELP -> Entity.Help
        }
    }
}

sealed class Entity (){
    //adding object
    object Help: Entity() {
        val name = "help"
    }


    data class Noob(val id: String, val name: String):Entity()
    data class Pro(val id: String, val name: String):Entity()
    data class Legend(val id: String, val name: String, val skill : Float):Entity()
}


fun Entity.Noob.printInfo(){
    println("this is noobs extention function $id")
}

val Entity.Pro.extVal: String
    get() = "Pro Extension"

fun main() {
    val entity1 = Entity.Noob("_id_noob",  "Noob master")
    val entity2 = EntityFactory.getEntity(PRO)

    //we can do this if we know the id value directly
    //Entity.Noob("id", "name").printInfo()

    //if we dont know the value we can cast it
    if (entity2 is Entity.Noob){
        entity2.printInfo()
    }
    if (entity2 is Entity.Pro){
        println(entity2.extVal)
    }

//
//    if (entity1 == entity2){
//        println("Both are equal")
//    }else{
//        println("Both are different")
//    }



}