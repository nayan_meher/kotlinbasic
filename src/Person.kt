class Person (val firstname: String= "noob",  val lastname: String = "master"){
    // first time declaration and assignment
    /*val firstname: String = _fistname
    val lastname: String= _lastname*/

   /* init {
        println("thi is first init")
    }

    constructor():this("hello", "master"){
        println("this is secondary constructor.")
    }

    init {
        println("This is second init")
    }*/

    //2nd typ initialization
    /*init {
        firstname = _fistname
        lastname = _lastname
    }*/

    //initialize a setter property with null

    var nickName: String? = null

        set(value) {
            field = value
            println("new nick name is : $value")
        }
        get() {
            println("this is returning the field value is $field")
            return field
        }

    fun showInfo(){
       // val nicknameToPrint = if (nickName != null) nickName else "no nick name"
        val nicknameToPrint = nickName ?: "no nickname"
        println("firstname: $firstname , lastname: $lastname, nickname: $nicknameToPrint")
    }

}